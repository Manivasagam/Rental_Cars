<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.util.*" %>
        <%@ page import="com.bussiness.Customer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Records</title>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cyborg/bootstrap.min.css" rel="stylesheet" type="text/css">
<style>
.container { margin:150px auto;}
</style>
<script>

function myFunction() {

window.open("insert.html","_self");

}

function doDelete()
{
	window.open("DeleteServlet","_self");

	}
</script>
</head>
<body>

<%

ArrayList<Customer> dataLst=(ArrayList<Customer>)request.getAttribute("Customer_info");

%>

<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></div>
<div class="jquery-script-clear"></div>
</div>
</div>
<div class="container">
<h1>Total Records in Rental Cars</h1>
<table class="table table-bordered">


    <thead>
      <tr>
       <th>Id</th>
<th>Source</th>
<th>Destination</th>
<th>Car</th>
<th>Price/hr</th>
<th></th>
<th><button id="bElim" type="button" class="btn btn-sm btn-default" target = "_self" onclick="myFunction()">
<span class="glyphicon glyphicon-plus" > </span>
</button></th>



      </tr>
    </thead>
    
    <%
for(int i=0;i<dataLst.size();i++)
{
	Customer c=dataLst.get(i);
%>
<tbody>
<tr>
<td> <%=i+1%></td>
<td> <%=c.getPick() %></td>
<td> <%=c.getDesti() %></td>
<td> <%=c.getCar() %></td>
<td> <%=c.getPrice() %></td>

<td><a href="UpdateServlet?id=<%=c.getId() %>"> <button id="bElim" type="button" class="btn btn-sm btn-default" target = "_self" 
onclick="doUpdate()">
<span class="glyphicon glyphicon-pencil" > </span>
</button></a>

<a href="DeleteServlet?id=<%=c.getId() %>"> <button id="bElim" type="button" class="btn btn-sm btn-default" target = "_self" 
onclick="doDelete()">
<span class="glyphicon glyphicon-trash" > </span>
</button></a></td>






</tr>
</tbody>
<%
}
%>	


  </table>
  </div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="bootstable.js"></script>

<script>
 $('table').SetEditable();
</script>
</body>
</html>