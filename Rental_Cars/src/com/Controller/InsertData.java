package com.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Out;

import com.bussiness.DataCollector;

/**
 * Servlet implementation class InsertData
 */
@WebServlet("/InsertData")
public class InsertData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PrintWriter out;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			String name=request.getParameter("name");
			String email=request.getParameter("email");
			String password=request.getParameter("password");
			String cpassword=request.getParameter("cpassword");
			
			
	
			
			
			System.out.println("name>>"+name);
			System.out.println("email>>"+email);
			System.out.println("pass>>"+password);
			System.out.println("cpass>>"+cpassword);
		
			
			
			DataCollector dc=new DataCollector();
			int status=dc.doInsert(name,email,password,cpassword);
			PrintWriter out=	response.getWriter();
			
			
			if(status>0)
			{
				out.println("<script type=\"text/javascript\">");
			       out.println("alert('Welcome "+name+" to Rental Cars');");
			       out.println("location='login.html';");
			       out.println("</script>");
				
			}
			else
			{
				out.println("<script type=\"text/javascript\">");
			       out.println("alert('Sign up Fails');");
			       out.println("location='login.html';");
			       out.println("</script>");
			      }
		}
		catch(Exception e)
		{
		System.out.print(e);
		e.printStackTrace();
		}

			}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
