package com.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bussiness.Customer;
import com.bussiness.DataCollector;

/**
 * Servlet implementation class InsertDataA
 */
@WebServlet("/InsertDataA")
public class InsertDataA extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertDataA() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		try
		{
		String pick = request.getParameter("pick");
		String desti = request.getParameter("desti");
		String car = request.getParameter("car");
		String price = request.getParameter("price");
		int Price = Integer.parseInt(price);
		
		System.out.println("name>>"+pick);
		System.out.println("email>>"+desti);
		System.out.println("pass>>"+car);
		System.out.println("cpass>>"+Price);
		
		DataCollector dc = new DataCollector();
		int a = dc.doInsert1(pick,desti,car,Price);
		System.out.println("Inserted sucessfully");
		PrintWriter out=	response.getWriter();
		
		
		if(a>0)
		{
			ArrayList<Customer> dataLst=dc.doview();
			System.out.println("size::"+dataLst.size());
			request.setAttribute("Customer_info", dataLst);
		RequestDispatcher rd=request.getRequestDispatcher("table.jsp");

		rd.forward(request, response);

		}
		else
		{
			out.println("<script type=\"text/javascript\">");
		       out.println("alert('insert Fails');");
		       out.println("location='insert.html';");
		       out.println("</script>");
		      }
		
	}
	catch(Exception e)
	{
	System.out.print(e);
	e.printStackTrace();
	}
		
	}

}
