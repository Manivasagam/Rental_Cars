package com.Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bussiness.Customer;
import com.bussiness.DataCollector;
import com.bussiness.Customer;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet("/BookServlet")
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	String p=request.getParameter("pickup");
	String d=request.getParameter("destination");
	String date=request.getParameter("date");
	
	System.out.println("p=="+p+"d=="+d);
	DataCollector dc=new DataCollector();
	ArrayList<Customer> dataLst=dc.dobook(p,d);
	System.out.println("size::"+dataLst.size());
	
	dc.docollect1(p, d, date);
	request.setAttribute("Customer_info", dataLst);
RequestDispatcher rd=request.getRequestDispatcher("index.jsp");

rd.forward(request, response);



	
	
	}

}
