package com.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bussiness.DataCollector;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	
	
	
	
	        PrintWriter out = response.getWriter();
	        
	        String email = request.getParameter("email");
	        String cpassword = request.getParameter("password");
	        DataCollector dc = new DataCollector();
	        boolean s = dc.doselect(email, cpassword);
	        
	        if(s)
	        {
	        	 out.println("<script type=\"text/javascript\">");
	        	 out.println("alert('Login Successful');");
			       out.println("location='book.html';");
			       out.println("</script>");
	           
	        }
	        else
	        {
	           
	        
	           out.println("<script type=\"text/javascript\">");
		       out.println("alert('Username or Password incorrect');");
		       out.println("location='login.html';");
		       out.println("</script>");
			
	        }
	    }  
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
