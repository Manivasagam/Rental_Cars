package com.bussiness;

import java.sql.Connection;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

public class DataCollector {

	public Connection dbConnection()
	{
		Connection con=null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/zoom?useSSL=false","root","root");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
	public  boolean doselect(String email,String cpassword) 
    {
     boolean st =false;
     try{
    	 	System.out.println("email=="+email+ "password=="+cpassword);
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();
			 PreparedStatement ps =(PreparedStatement) con.prepareStatement
                     ("select * from car1 where email=? and cpassword=?");
 ps.setString(1, email);
 ps.setString(2, cpassword);
 ResultSet rs =ps.executeQuery();
 st = rs.next();
 con.close();

}catch(Exception e)
{
  e.printStackTrace();
}
 return st;                 
}   
	
	public int  doInsert(String name,String email,String password,String cpassword)
	
	{

		int resultStatus=-1;
		try {
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();
			
			String query="insert into car1 values('"+name+"','"+email+"','"+password+"','"+cpassword+"')";
			System.out.println("qry::"+query);
			 resultStatus=stmt.executeUpdate(query);
			System.out.println("status::"+resultStatus);
con.close();
			}
			catch(Exception e)
			{
			System.out.print(e);
			e.printStackTrace();
			}
			
		
	      return resultStatus;
		
	}


	public  ArrayList<Customer> dobook(String p,String d)
	{
		
		ArrayList<Customer> dataLst=new ArrayList<Customer>();
		try {
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();
			 PreparedStatement ps =(PreparedStatement) con.prepareStatement
                     ("select * from trip where pick=? and desti=?");
			 ps.setString(1, p);
			 ps.setString(2, d);
			ResultSet rs = ps.executeQuery();
		while(rs.next())
		{

			
			String Source = rs.getString(2);
			String Destination = rs.getString(3);
			String Car = rs.getString(4);
			String Price = rs.getString(5);
			
			Customer c=new Customer();
			
			c.setPick(Source);
			c.setDesti(Destination);
			c.setCar(Car);
			c.setPrice(Price);
			
			
			
			
			dataLst.add(c);
			System.out.println("::"+Source+":"+Destination+":"+Car+":"+Price);
		}
		
		con.close();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataLst;

		
	}
	
	public  ArrayList<Customer> doview()
	{
		
		ArrayList<Customer> dataLst=new ArrayList<Customer>();
		try {
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();
	
			ResultSet rs = stmt.executeQuery ("select * from trip");
		
		while(rs.next())
		{

			int Id = rs.getInt(1);
			String Source = rs.getString(2);
			String Destination = rs.getString(3);
			String Car = rs.getString(4);
			String Price = rs.getString(5);
			
			Customer c=new Customer();
			c.setId(Id);
			c.setPick(Source);
			c.setDesti(Destination);
			c.setCar(Car);
			c.setPrice(Price);
			
			
			
			
			dataLst.add(c);
			System.out.println("::"+Id+":"+Source+":"+Destination+":"+Car+":"+Price);
		}
		
		con.close();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataLst;

		
	}
	
	public int  doInsert1(String pick,String desti,String car,int Price)
	{
		int resultStatus=-1;
		try {
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();
			
			String query="insert into trip (pick,desti,car,price) values('"+pick+"','"+desti+"','"+car+"','"+Price+"')";
			System.out.println("qry::"+query);
			 resultStatus=stmt.executeUpdate(query);
			System.out.println("status::"+resultStatus);

			}
			catch(Exception e)
			{
			System.out.print(e);
			e.printStackTrace();
			}
			
		
	      return resultStatus;
		
		}
	
	public void doDelete(int id) 
	{
		Connection con = dbConnection();
		try {
			Statement stmt =con.createStatement();
			int i=stmt.executeUpdate("DELETE FROM trip WHERE id="+id);
			System.out.println("Data Deleted Successfully!");
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public  ArrayList docollect()
	{
		
		ArrayList dataLst=new ArrayList();
		try {
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();
	
			ResultSet rs = stmt.executeQuery ("select * from car1");
		
		while(rs.next())
		{

			
			String name = rs.getString(1);
			String email = rs.getString(2);
		
			
		
		dataLst.add(name);
		dataLst.add(email);
			
			
			
			System.out.println("::"+name+":"+email);
		}
		
		con.close();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataLst;

		
	}
	
	
	public int  docollect1(String pick,String desti,String date)
	{
		int resultStatus=-1;
		try {
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();
			
			String query="insert into summary values('"+pick+"','"+desti+"','"+date+"')";
			System.out.println("qry::"+query);
			 resultStatus=stmt.executeUpdate(query);
			System.out.println("status::"+resultStatus);

			}
			catch(Exception e)
			{
			System.out.print(e);
			e.printStackTrace();
			}
			
		
	      return resultStatus;
		
		}
	
	public  ArrayList doView()
	{
		
		ArrayList dataLst=new ArrayList();
		try {
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();
	
			ResultSet rs = stmt.executeQuery ("select * from summary");
		
		while(rs.next())
		{

			
			String p = rs.getString(1);
			String d = rs.getString(2);
			String date = rs.getString(3);

			
		
		dataLst.add(p);
		dataLst.add(d);
		dataLst.add(date);
			
			
			
			System.out.println("::"+p+":"+d+":"+date);
		}
		
		con.close();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataLst;

		
	}
	
	
	
	
	
}


